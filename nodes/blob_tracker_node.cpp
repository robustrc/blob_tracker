/*
Software License Agreement (BSD License)
Copyright (c) 2018, Fredrik Macintosh
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided
with the distribution.
* Neither the name of blob_tracker nor the names of its
contributors may be used to endorse or promote products derived
from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
author: Fredrik Macintosh
*/

// Takes images from a camera, sets up a thresholding window to configure the parameters live.
// Thresholds the image, filters out blobs by area then does an adaptive search area
// to find clusters with a sort of specific pattern. Sorts the patterns by predefined
// structure then calculates forward and get the mass centroids x,y position and rotation

// PUBLISHED TOPICS: /car#PoseFromCam where # is the id of the car (supports 2 cars at the moment)
// Note that the current coordinate system has [0,0] in the upper left corner and the angle is 0 (2pi)
// horisontally to the right so to speak.
// SUBSCRIBED TOPIC: Camera feed of your choice. This particular example uses the flea3 package to
// stream from a FLIR Point Grey Flea3 camera.
// https://github.com/KumarRobotics/flea3
//

//NOTE: To compile you might have to manually link some libraries because my development
// environment is broken, heh.
//

// Note to reader is that I am in no way a programmer and have limited experience in c++.
// If you see something implemented stupidly implemented or incorrectly, you're most likely
// right. I'm more of a make it work kind of guy. / Fredrik

#include "../include/methods.h"


// I like global variables and I haven't bothered to learn pointers.
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
std::vector<geometry_msgs::Vector3Stamped> carPoses;
cv::SimpleBlobDetector::Params params;
cv::Mat im;
cv::Mat imCopy;
cv::Mat imWithPoints;
std::vector<cv::Mat> subImages;


bool runOnce = true;
bool visualize;
cv::Point offset;
std::vector<cv::Point> points;
std::vector<std::vector<cv::Point> > contours;

// Set som initial values for tracking, will most likely have to be adjusted.
int minArea = 2000;
int maxArea = 4400;
int blobSize = 17;
int subImageSize = 46;
int threshold = 230;

float viewPortX = 4.29; // Width of the camera view, in meters.
float viewPortY = 3.45; // Height of the camera view, in meters.
double dt = 0;


ros::Publisher car0PosePub;
ros::Publisher car1PosePub;

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// END OF VARIABLE DECLARATION
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void imageCb(const sensor_msgs::ImageConstPtr& msg)
{
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
    // Read image
    im = cv_ptr->image;
    u_int camPixelsWidth = im.cols; // Get width of image feed
    u_int camPixelsHeight = im.rows; // You get the idea


    float px2mWidth = viewPortX/camPixelsWidth; // Used to convert from pixel position to real world position
    float px2mHeight = viewPortY/camPixelsHeight; //

        // Create some trackbars to adjust parameters
        // NOTE: If image from cam is black and white, leave Hue and Saturation at 0.
        /*
        cv::namedWindow("Control", 1);
        //cv::namedWindow("MEGA BLOBS", 1);
        //cv::namedWindow("Thresholded image", 1);
        cv::waitKey(1);
        cv::createTrackbar( "minArea" , "Control", &minArea, 8000);
        cv::createTrackbar( "maxArea" , "Control", &maxArea, 8000);
        cv::createTrackbar( "Thresholding" , "Control", &threshold, 255);
        cv::createTrackbar( "blobSize" , "Control", &blobSize, 30);
        cv::createTrackbar( "subImageSize" , "Control", &subImageSize, 100);
        */


    // Make sure we actually managed to get a frame, otherwise shit would break.
    if (im.rows > 10 && im.cols > 10)
    {
        //NOTE: If you want to onvert the image, this is the wei.
        im =  cv::Scalar::all(255) - im;

        // Threshold image
        cv::inRange(im, 0,threshold,im);
        //cv::dilate(im, im, cv::Mat(), cv::Point(-1, -1), 1, 1, 1);
        cv::erode(im, im, cv::Mat(), cv::Point(-1, -1), 1, 1, 1);
        // Create another copy of the thresholded image and turn the blob clusters
        // into MEGA BLOBS™ for use with the adaptive search later.
        im.copyTo(imCopy);
        cv::dilate(imCopy, imCopy, cv::Mat(), cv::Point(-1, -1), blobSize, 1, 1);
        cv::erode(imCopy, imCopy, cv::Mat(), cv::Point(-1, -1), 2, 1, 1);

        // Visualization stuff
/*
        cv::imshow("MEGA BLOBS", imCopy);
        cv::imshow("Thresholded image",im);
        cv::waitKey(1);
        cv::waitKey(1);
        */

        // /////////////////////////////////////////////

        // Find the steamingly fresh MEGA BLOBS™
        cv::findContours(imCopy, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
        std::vector<std::vector<cv::Point> > filtContours;

        // Filter the blobs by area, if we get YUGE (Think Trump yuge) areas, it's most likely
        // glare from the floor, so we don't want that.
        // If they're too small, it's most likely alien IR dust. We don't want that either.
        // Area is an area (heh) of configuration.
        for (size_t i = 0; i < contours.size(); i++) {
            int blobArea = cv::contourArea(contours[i]);
            if ((blobArea > minArea) && (blobArea < maxArea)) {
                filtContours.push_back(contours[i]);
            }
        }
        points = getCenters(filtContours);
        // Create a subimage centered around each found blob.
        if (points.size() > 0)
        {
            std::vector<SubImage> subImages = createSubImages(points,im,subImageSize*2);
            if (subImages.size() > 0) {
                carPoses = getCars(subImages, px2mWidth, px2mHeight, viewPortY);
            }
        }
        // Publish everything. Placed them here to avoid duplicate data.
        for (size_t i = 0; i < carPoses.size(); i++)
        {
            if (carPoses[i].header.frame_id == "0") {
                if (true || !garbageFlag[0])
                {
                    car0PosePub.publish(carPoses[i]);
                }
                else if (carPoses[i].header.frame_id == "1")
                {
                    if (!garbageFlag[1]) {
                        car1PosePub.publish(carPoses[i]);
                    }
                }
            }
        }
    }
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "blob_tracker");
    ros::NodeHandle nh_;
    ros::NodeHandle n;
    ros::Rate loop_rate(150);
    cv::Point2f centroid;
    /*
    cv::namedWindow("Image feed", 1);
    cv::waitKey(1);
    cv::namedWindow("Thresholded image",1);
    cv::waitKey(1);
    cv::namedWindow("MEGA BLOBS",1);
    cv::waitKey(1);
    */

    image_transport::Subscriber image_sub_;

    car0PosePub = n.advertise<geometry_msgs::Vector3Stamped>("car0PoseFromCam", 1);
    car1PosePub = n.advertise<geometry_msgs::Vector3Stamped>("car1PoseFromCam", 1);

    image_transport::ImageTransport it_(nh_);
    image_sub_ = it_.subscribe("/pg_14506672/image_raw", 1, imageCb);

    while (ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
