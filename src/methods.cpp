#include "../include/methods.h"

// This entire thing could probably be sped up by the use of global variables and pointers.
// Right now, all of the variables are re-instanciated on each run of a method.
// Probably isn't the fastest.....



// This thing allows us to sort a struct by one of its members.
bool distCompare(PointWithDist i,PointWithDist j)
{
    return (i.distance<j.distance);
}

geometry_msgs::Vector3Stamped pose;
geometry_msgs::Vector3Stamped lastCarPose;
std::vector<bool> garbageFlag(2);
float poseX;
float poseY;
float yaw;
cv::Point forward;
cv::Point cent;
cv::Point mc; // Note: this variable is unused.
bool firstPose = true;
// Calculates the pose (relative the camera) of the triangularly positioned blobs by finding the forward one
// , creating a vector from the centroid to it and calculating the angle of that.
// Position given is the position of the centroid.

geometry_msgs::Vector3Stamped getPose(std::vector<cv::Point> points, std::string carId, float px2mWidth, float px2mHeight, cv::Point offset, float viewPortY)
{

    forward = getForwardPoint(points);
    poseX = px2mWidth*(getCentroid(points).x + offset.x);
    // Flip y coordinate to better suit EVERY BLOODY CARTHESIAN SYSTEM EVER??!!
    poseY = viewPortY - px2mHeight*(getCentroid(points).y + offset.y);
    if (firstPose) {
        lastCarPose.vector.x = poseX;
        lastCarPose.vector.y = poseY;
        firstPose = false;
    }

    if(isDataGarbage(poseX, poseY, lastCarPose))
    {
        garbageFlag[atoi(carId.c_str())] = true;
        pose.header.frame_id = "DATA INVALID";
    }
    else
    {
        pose.vector.x = poseX;
        pose.vector.y = poseY;

        // This part is a bit filthy. Send a vector3 with x y and yaw...
        // This is done because I'm too lazy to write a proper ros message for sending stamped
        // RPY angles ¯\_(ツ)_/¯ . Sue me. /Fredrik
        pose.vector.z = atan2(pose.vector.y-( viewPortY - px2mHeight*( forward.y + offset.y)), pose.vector.x-px2mWidth*(forward.x + offset.x))+M_PI;
        pose.header.stamp = ros::Time::now();
        pose.header.frame_id = carId;
        lastCarPose = pose;
        garbageFlag[atoi(carId.c_str())] = false;
    }


    return pose;
}

// Fetches the centroid of the three detected dots by use of simple mean calculation.
// Nothing fancy
cv::Point getCentroid(std::vector<cv::Point> points)
{
    int size = points.size();
    // Initilize the centroid points to zero to avoid 'junk at the adress' bugs.
    cent.x = 0;
    cent.y = 0;
    for (size_t i = 0; i < size; i++) {
        cent.x += points[i].x;
        cent.y += points[i].y;
    }
    cent.x = cent.x/size;
    cent.y = cent.y/size;
    return cent;
}
//Calculates the mass center of each detected contour and returns them
// Again, nothing fancy
std::vector<cv::Point> getCenters(std::vector<std::vector<cv::Point> > contours)
{
    std::vector<cv::Point> points(contours.size());
    std::vector<cv::Moments> mu(contours.size() );
    for( int i = 0; i < contours.size(); i++ )
    {
        mu[i] = moments( contours[i], false );
        points[i] = cv::Point( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 );
    }
    return points;
}
// Get the forward point by simple distance calculation and logic.
// The distance hysteresis will most likely have to be tuned.
cv::Point getForwardPoint(std::vector<cv::Point> points)
{
    cv::Point forward;
    int hysteresis = 4;
    int dist1 = cv::norm(points[0]-points[1]);
    int dist2 = cv::norm(points[0]-points[2]);

    if (std::fabs(dist1 - dist2) < hysteresis )
    {
        forward = points[0];
    }
    // TODO: This is broken and needs to be fixed.
    else if ((dist1 - dist2) < -hysteresis)
    {
        forward = points[2];
    }
    else
    {
        forward = points[1];
    }
    return forward;
}

// Creates sub-images for the adaptive search area.
std::vector<SubImage> createSubImages(std::vector<cv::Point> points, cv::Mat src, int subImageSize)
{
    int nrOfBlobs = points.size();
    subImageSize = subImageSize/2;
    // Visualization stuff
     //
     // cv::namedWindow("car0Track",1);
     // cv::waitKey(1);
     //
     // cv::namedWindow("car1Track",1);
     // cv::waitKey(1);
     //
    //////////////////////////////////////
    std::vector<SubImage> subImages(nrOfBlobs);
    // To make sure we don't break shit by accidentally creating a sub image in
    // a random location in memory, check that the image will fit around the
    // located point, if not. Move it.
    for (size_t i = 0; i < nrOfBlobs; i++) {
        // Is x too small?
        if (points[i].x < subImageSize)
        {
            // Set upperLeftCorner.x to 0;

            subImages[i].upperLeftCorner.x = 0;
            // Check possible y cases
            if (points[i].y < subImageSize)
            {
                subImages[i].upperLeftCorner.y = 0;
            }
            else if (points[i].y > src.rows - subImageSize)
            {
                subImages[i].upperLeftCorner.y = src.rows - (2*subImageSize);
            }
            else
            {
                subImages[i].upperLeftCorner.y = points[i].y-subImageSize;
            }
        }
        // Is x too large?
        else if (points[i].x > src.cols - subImageSize)
        {
            // Set upperLeftCorner.x to width of picture - width of subimage
            subImages[i].upperLeftCorner.x = src.cols - (2*subImageSize);
            if (points[i].y < subImageSize)
            {
                subImages[i].upperLeftCorner.y = 0;
            }
            else if (points[i].y > src.rows - subImageSize)
            {
                subImages[i].upperLeftCorner.y = src.rows - (2*subImageSize);
            }
            else
            {
                subImages[i].upperLeftCorner.y = points[i].y-subImageSize;
            }
        }
        else
        {
            // Is X ok but Y not?
            // Set upperLeftCorner.x to where it was found
            subImages[i].upperLeftCorner.x = points[i].x - subImageSize;
            if (points[i].y < subImageSize) {
                subImages[i].upperLeftCorner.y = 0;
            }
            else if (points[i].y > src.rows - subImageSize)
            {
                subImages[i].upperLeftCorner.y = src.rows - (2*subImageSize);
            }
            else // Are both ok?
            {
                subImages[i].upperLeftCorner.y = points[i].y - subImageSize;
            }
        }
        src(cv::Rect(subImages[i].upperLeftCorner.x,subImages[i].upperLeftCorner.y, subImageSize*2,subImageSize*2)).copyTo(subImages[i].image);
    }
    // Visualization stuff
/*
    for (size_t i = 0; i < subImages.size(); i++) {
        switch (i) {
            case 0:
                       cv::imshow("car0Track",subImages[i].image);
                        cv::waitKey(1);
            break;
            case 1:
                        cv::imshow("car1Track",subImages[i].image);
                        cv::waitKey(1);
            break;
        }
    }
    */

    /////////////////////////////////////////
    return subImages;
}
// Get the cars pose and set their ID.
std::vector<geometry_msgs::Vector3Stamped> getCars(std::vector<SubImage> subImages, float px2mWidth, float px2mHeight, float viewPortY)
{
    std::vector<geometry_msgs::Vector3Stamped> cars;
    // For every subImage, look for the small tracker dots
    for (size_t i = 0; i < subImages.size(); i++)
    {
        std::vector<std::vector<cv::Point> > contours;
        std::vector<cv::Point> points;
        cv::findContours(subImages[i].image, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
        // ID car by number of found dots. At this point, data is assumed
        // to be noise free enough for this to work.
        switch (contours.size())
        {
            case 4:
            points = getCenters(contours);
            cars.push_back( getPose(getTriangle(points),"0", px2mWidth,
            px2mHeight, subImages[i].upperLeftCorner, viewPortY) );
            break;

            case 5:
            points = getCenters(contours);
            cars.push_back( getPose(getTriangle(points),"1", px2mWidth,
            px2mHeight, subImages[i].upperLeftCorner, viewPortY) );
            break;
        }
    }
    return cars;
}
// Filters out the identifying dots from the center of the tracker cluster and assumes that the remaining
// points are the triangle we're looking for for the actual tracking part.
std::vector<cv::Point> getTriangle(std::vector<cv::Point> points)
{
    std::vector<cv::Point> triangle;
    cv::Point centroid = getCentroid(points);
    std::vector<PointWithDist> distances(points.size());
    // Calculate the distance to all points from the centroid.
    for (size_t i = 0; i < points.size(); i++) {
        distances[i].distance = (cv::norm(centroid-points[i]));
        distances[i].point = points[i];
    }
    // Sort the points according to their distance to the center and save the
    // three points that are the furthest away from the center.
    std::sort(distances.begin(),distances.end(),distCompare);
    for (size_t i = distances.size()-3; i < distances.size(); i++) {
        triangle.push_back(distances[i].point);
    }
    return triangle;
}

bool isDataGarbage(float poseX, float poseY, geometry_msgs::Vector3Stamped lastCarPose)
{
    if ((poseX < 0) || (poseX > 4) || ((poseX - fabs(lastCarPose.vector.x)) > 0.5) || (poseY < 0) || (poseY > 4) || ((poseY - fabs(lastCarPose.vector.y)) > 0.5))
    {
        return true;
    }
    else
    {
        return false;
    }
}
