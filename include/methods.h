

#ifndef ITS_2018_I_SHOULD_NOT_HAVE_TO_DO_THIS_HOLY_SHIT
#define ITS_2018_I_SHOULD_NOT_HAVE_TO_DO_THIS_HOLY_SHIT


// Some of these imports are probably unnecessary...lord forgive me
#include "/opt/ros/lunar/include/geometry_msgs/Vector3Stamped.h"
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <sensor_msgs/image_encodings.h>
#include "/opt/ros/lunar/include/ros/ros.h"
#include "/opt/ros/lunar/include/rospack/rospack.h"
#include "/opt/ros/lunar/include/tf/tf.h"
#include <image_transport/image_transport.h>
#include <math.h>
#include <fenv.h>
#include <iostream>
#include <string>


struct SubImage
{
    cv::Mat image;
    cv::Point upperLeftCorner;
};

struct PointWithDist
{
    cv::Point point;
    float distance;
};

extern geometry_msgs::Vector3Stamped lastCarPose;
extern std::vector<bool> garbageFlag;

void imageCb(const sensor_msgs::ImageConstPtr& msg);

geometry_msgs::Vector3Stamped getPose(std::vector<cv::Point> points, int carId,
    float px2mWidth, float px2mHeight, cv::Point offset, float viewPortY);

std::vector<cv::Point> getCenters(std::vector<std::vector<cv::Point> > contours);

cv::Point getCentroid(std::vector<cv::Point> points);

cv::Point getForwardPoint(std::vector<cv::Point> points);

std::vector<SubImage> createSubImages(std::vector<cv::Point> points, cv::Mat src,int subImageSize);

std::vector<geometry_msgs::Vector3Stamped> getCars(std::vector<SubImage> subImages,
    float px2mWidth, float px2mHeight, float viewPortY);

std::vector<cv::Point> getTriangle( std::vector<cv::Point> points);

bool isDataGarbage(float poseX, float poseY, geometry_msgs::Vector3Stamped lastCarPose);




#endif
