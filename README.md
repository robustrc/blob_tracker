
# What is this repository for? #

* Tracking specifically placed IR LEDs or dots, identifying them and publishing their [X,Y] position and euler angle (yaw).

## How do I get set up? ##

### Requirements ###

* ROS Lunar Logerhead
* OpenCV2, not sure what happens should you have OpenCV3 installed...
* Some type of camera, this specific project uses a FLIR Point Grey Flea3 USB cam. Any camera should do, but with variying performance.
* To run the camera, you'll need to install drivers from here: https://github.com/RhobanDeps/flycapture You also might have a few dependency issues, but that should pop up and be a simple fix.

## Running the thing ##
This repo provides a single node. blob_tracker_node.
to run it, simply start the camera providing node of your choice, making sure that blob_tracker_node actually subscribes to its published topic.
then run
> rosrun blob_tracker blob_tracker_node

and let it do its magic.

There are a few tunable parameters hidden around the code, horrible structure yes. Like hystersis and stuff like that, have a look around..

# Who do I talk to? #

* Fredrik Macintosh, fredrik.macintosh@gmail.com
* Other community or team contact